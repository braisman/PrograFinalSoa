package com.test.abcbr.sporttec.sport;

import com.test.abcbr.sporttec.models.Sport;

/**
 * Created by abcbr on 26/11/2017.
 */

public interface OnSportItemSelected {

    void onSportItemSelected(Sport item);
}
