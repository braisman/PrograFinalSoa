package com.test.abcbr.sporttec.sport;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.models.Sport;

import com.test.abcbr.sporttec.R;

import java.util.List;

/**
 *
 */
public class MySportItemRecyclerViewAdapter extends RecyclerView.Adapter<MySportItemRecyclerViewAdapter.ViewHolder> {

    private final List<Sport> mValues;
    private final OnSportItemSelected mListener;

    public MySportItemRecyclerViewAdapter(List<Sport> items, OnSportItemSelected listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_sportitem2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        Ion.with(holder.mImageView).placeholder(R.drawable.sportteclogo)
                .error(R.drawable.sportteclogo).load(holder.mItem.getmImageUrl());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSportItemSelected(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public Sport mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.sportitem_img);
        }
    }
}
