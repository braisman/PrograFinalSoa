package com.test.abcbr.sporttec.models;


import com.google.gson.annotations.SerializedName;

/**
 * Created by abcbr on 25/11/2017.
 */

public class Sport {

    public static final String SPORT_PATH = ":3000/api/Sports";

    public static final String SPORT_NAME = "name";

    public static final String SPORT_IMAGE = "image_url";

    @SerializedName(SPORT_NAME)
    public String mName;
    @SerializedName(SPORT_IMAGE)
    public String mImageUrl;

    public Sport() {
    }

    public Sport(String mName, String mIconUrl) {
        this.mName = mName;
        this.mImageUrl = mIconUrl;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mIconUrl) {
        this.mImageUrl = mIconUrl;
    }
}
