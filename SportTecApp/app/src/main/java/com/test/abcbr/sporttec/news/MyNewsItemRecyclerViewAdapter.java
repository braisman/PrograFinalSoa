package com.test.abcbr.sporttec.news;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.FragmentListener;
import com.test.abcbr.sporttec.models.New;

import com.test.abcbr.sporttec.R;

import java.util.List;

/**
 *
 * TODO: Replace the implementation with code for your data type.
 */
public class MyNewsItemRecyclerViewAdapter extends RecyclerView.Adapter<MyNewsItemRecyclerViewAdapter.ViewHolder> {

    private final List<New> mValues;
    private final FragmentListener mListener;

    public MyNewsItemRecyclerViewAdapter(List<New> items, FragmentListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return 1;
        }
        return 2;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_newsitem_first, parent, false);
            return new ViewHolder(view, viewType);
        }
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_newsitem, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.mTxtvTitle.setText(mValues.get(position).getmTitle());
        if(holder.mViewType == 2){
            holder.mTxtvDate.setText(mValues.get(position).getmDate());
        }

        Ion.with(holder.mImg).placeholder(R.drawable.sportteclogo)
                .error(R.drawable.sportteclogo).load(holder.mItem.getmImageUrl());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.loadNew(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTxtvTitle;
        public final TextView mTxtvDate;
        public final ImageView mImg;
        public New mItem;
        public final int mViewType;

        public ViewHolder(View view, int viewType) {
            super(view);
            mView = view;
            mImg = view.findViewById(R.id.new_item_img);
            mTxtvTitle = (TextView) view.findViewById(R.id.new_item_title);
            mViewType = viewType;
            if(viewType == 1){
                mTxtvDate = (TextView) view.findViewById(R.id.new_item_date);
            } else {
                mTxtvDate = (TextView) view.findViewById(R.id.new_item_date);
            }

        }
    }
}
