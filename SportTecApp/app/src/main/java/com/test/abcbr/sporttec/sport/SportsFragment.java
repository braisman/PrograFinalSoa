package com.test.abcbr.sporttec.sport;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.R;
import com.test.abcbr.sporttec.connection.ConnectionManager;
import com.test.abcbr.sporttec.models.Sport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SportsFragment extends Fragment implements OnSportItemSelected {

    private int mColumnCount = 2;

    private RecyclerView recyclerView;

    private OnSportItemSelected listener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SportsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sportitem_list, container, false);



        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            listener = this;
            Ion.with(getContext())
                    .load("GET", "http://"+ ConnectionManager.getIP()+Sport.SPORT_PATH)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray result) {
                            Gson gson = new Gson();
                            Sport sports_array[] = gson.fromJson(result, Sport[].class);
                            List<Sport> sports = new ArrayList<>(Arrays.asList(sports_array));
                            recyclerView.setAdapter(new MySportItemRecyclerViewAdapter(sports, listener));
                        }
                    });
        }
        return view;
    }

    @Override
    public void onSportItemSelected(Sport item) {

    }
}
