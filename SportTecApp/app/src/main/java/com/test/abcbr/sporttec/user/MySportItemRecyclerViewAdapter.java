package com.test.abcbr.sporttec.user;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.models.Sport;

import com.test.abcbr.sporttec.R;

import java.util.List;

/**
 *
 */
public class MySportItemRecyclerViewAdapter extends RecyclerView.Adapter<MySportItemRecyclerViewAdapter.ViewHolder> {

    private final List<Sport> mValues;
    private final OnSportItemSelected mListener;

    public MySportItemRecyclerViewAdapter(List<Sport> items, OnSportItemSelected listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_sportitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        Ion.with(holder.mImgView).placeholder(R.drawable.ic_menu_sports)
                .error(R.drawable.ic_image_error).load(holder.mItem.getmImageUrl());

        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mListener.onItemSportCheckedChange(holder.mItem, isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImgView;
        public final CheckBox mCheckBox;
        public Sport mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImgView = mView.findViewById(R.id.sportitem_img);
            mCheckBox = mView.findViewById(R.id.sportitem_checkbox);
        }
    }
}
