package com.test.abcbr.sporttec.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abcbr on 26/11/2017.
 */

public class User {

    public static final String USER_PATH = ":3000/api/UsersApp";

    public static final String USER_NAME = "name";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_IMG = "img";
    public static final String USER_SPORTS = "sports";
    public static final String USER_ID = "id";

    @SerializedName(USER_NAME)
    public String mName;
    @SerializedName(USER_EMAIL)
    public String mEmail;
    @SerializedName(USER_PASSWORD)
    public String mPassword;
    @SerializedName(USER_IMG)
    public String mImgUrl;
    @SerializedName(USER_SPORTS)
    public List<String> mSports;
    @SerializedName(USER_ID)
    public String mId;

    public User() {
    }

    public User(String mName, String mEmail, String mImgUrl, String[] sports, String mId) {
        this.mName = mName;
        this.mEmail = mEmail;
        this.mImgUrl = mImgUrl;
        this.mSports = new ArrayList<>(Arrays.asList(sports));
        this.mId = mId;
    }

    public JsonObject getJsonPost(){
        JsonObject user = new JsonObject();

        user.addProperty(USER_NAME, mName);
        user.addProperty(USER_PASSWORD, mPassword);
        user.addProperty(USER_EMAIL, mEmail);
        user.addProperty(USER_IMG, mImgUrl);

        JsonArray sports = new JsonArray();

        if(mSports != null){
            for (int i = 0; i < mSports.size(); i++) {
                sports.add(new JsonPrimitive(mSports.get(i)));
            }
            user.add(USER_SPORTS, sports);
        }

        return user;
    }
}
